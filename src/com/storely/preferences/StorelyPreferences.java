/**
 * 
 */
package com.storely.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * @author Gomathivinayagam Muthuvinayagam
 *
 */
public class StorelyPreferences {

	private static SharedPreferences pref;
	private static int PRIVATE_MODE = 0;
	
	public final static String PREF_NAME = "StorelyPref";
	public final static String KEY_NAME = "name";
	public final static String KEY_MAILID = "mailid";
	public final static String KEY_STORE_NAME = "storename";
	public final static String KEY_STORE_ID = "storeid";
	
	public static void initialize(Context context)
	{
		pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		StorelyPreferences.setProperty(KEY_STORE_ID, "saravana");
	}
	
	public static String getPropertyAsString(String key)
	{
		return pref.getString(key, null);
	}
	
	public static int getPropertyAsInt(String key)
	{
		return pref.getInt(key, -1);
	}
	
	public static boolean exists(String key)
	{
		return pref.contains(key);
	}
	
	public static void setProperty(String key, String val)
	{
		Editor editor = pref.edit();
		editor.putString(key, val);
		editor.commit();
	}
	
	public static void setProperty(String key, int val)
	{
		Editor editor = pref.edit();
		editor.putInt(key, val);
		editor.commit();
	}
}
