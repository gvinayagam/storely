/**
 * 
 */
package com.storely.database;

import android.content.ContentResolver;
import android.content.Context;

/**
 * @author Gomathivinayagam Muthuvinayagam
 *
 */

// This class provides a database helper object
public class DatabaseManager {
	
	private static DatabaseManager instance;
	private DatabaseHelper databaseHelper;
	private Context mContext;
	
	private DatabaseManager(Context context)
	{
		databaseHelper = new DatabaseHelper(context);
		mContext = context;
	}
	
	// initialize method is supposed to be called during the application launch
	public synchronized static void initialize(Context context)
	{
		if(null == instance)
		{
			instance = new DatabaseManager(context);
		}
	}
	
	public static DatabaseManager getInstance()
	{
		return instance;
	}
	
	public DatabaseHelper getHelper()
	{
		return databaseHelper;
	}
	
	public ContentResolver getContentResolver()
	{
		return mContext.getContentResolver();
	}

}
