/**
 * 
 */
package com.storely.database;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

/**
 * @author Gomathivinayagam Muthuvinayagam
 *
 */

// This class is used to generate the ormlite_config file which will be used by ORMLite framework
// This class will not run on android device
public class DatabaseORMLiteUtil extends OrmLiteConfigUtil {
	
	public static void main(String[] args) throws Exception
	{
		writeConfigFile("ormlite_config.txt");
	}

}
