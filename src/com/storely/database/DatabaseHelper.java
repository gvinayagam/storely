/**
 * 
 */
package com.storely.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.storely.R;
import com.storely.entities.app.AppProperty;
import com.storely.entities.stores.Address;
import com.storely.entities.stores.Product;
import com.storely.entities.stores.ProductImage;
import com.storely.entities.stores.User;


/**
 * @author Gomathivinayagam Muthuvinayagam
 *
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	private static final String DB_NAME="storely_data";
	private static final int DB_VERSION = 4;
	private static final String LOG_NAME = DatabaseHelper.class.getName();
	
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION,R.raw.ormlite_config);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
		// TODO Auto-generated method stub
		try {
			TableUtils.createTable(connectionSource, Address.class);
			TableUtils.createTable(connectionSource, User.class);
			TableUtils.createTable(connectionSource, Product.class);
			TableUtils.createTable(connectionSource, AppProperty.class);
			TableUtils.createTable(connectionSource, ProductImage.class);
			Log.i("DatabaseHelper","Tables are created");
		} catch (SQLException e) {
			Log.e(LOG_NAME, "Unable to create datbases", e);
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion,
			int newVersion) {
		// TODO Auto-generated method stub
		try {
			TableUtils.dropTable(connectionSource, Address.class,true);
			TableUtils.dropTable(connectionSource, User.class,true);
			TableUtils.dropTable(connectionSource, Product.class,true);
			TableUtils.dropTable(connectionSource, AppProperty.class,true);
			TableUtils.dropTable(connectionSource, ProductImage.class,true);
			
			onCreate(database,connectionSource);
			
		} catch (SQLException e) {
			Log.e(LOG_NAME, "Unable to upgrade database from version " + oldVersion + " to new "
					+ newVersion, e);
		}
		
	}
}
