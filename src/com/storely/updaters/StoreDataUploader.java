/**
 * 
 */
package com.storely.updaters;

import java.util.ArrayList;
import java.util.List;

import com.storely.entities.dao.AddressDaoWrapper;
import com.storely.entities.dao.ProductDaoWrapper;
import com.storely.entities.dao.UploadableDaoWrapper;
import com.storely.entities.dao.UserDaoWrapper;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
@SuppressWarnings("rawtypes")
public class StoreDataUploader {

	private static List<UploadableDaoWrapper> uploadableObjs;
	
	static
	{
		uploadableObjs = new ArrayList<UploadableDaoWrapper>();
		
		uploadableObjs.add(AddressDaoWrapper.getInstance());
		uploadableObjs.add(ProductDaoWrapper.getInstance());
		uploadableObjs.add(UserDaoWrapper.getInstance());
	}
	
	public static void uploadAppData()
	{
		/*for(UploadableDaoWrapper udwObj : uploadableObjs)
		{
			
		}*/
	}
	
}
