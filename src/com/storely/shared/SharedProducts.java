/**
 * 
 */
package com.storely.shared;

import android.util.SparseArray;

import com.storely.entities.stores.Product;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class SharedProducts {

	private static SparseArray<Product> positionAndProducts = new SparseArray<Product>();
	
	public static void put(int position, Product product)
	{
		positionAndProducts.put(position, product);
	}
	
	public static Product get(int position)
	{
		return positionAndProducts.get(position);
	}
	
	public static void remove(int position)
	{
		positionAndProducts.remove(position);
	}
}
