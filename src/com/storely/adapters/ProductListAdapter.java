/**
 * 
 */
package com.storely.adapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.storely.R;
import com.storely.entities.dao.ProductDaoWrapper;
import com.storely.entities.dao.ProductImageDaoWrapper;
import com.storely.entities.stores.Product;
import com.storely.entities.stores.ProductImage;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 * 
 */
public class ProductListAdapter extends CursorAdapter {

	private LayoutInflater inflater;

	public static ProductListAdapter getInstance(Context context) {
		return new ProductListAdapter(context, ProductDaoWrapper.getInstance()
				.getCursor());
	}

	private ProductListAdapter(Context context, Cursor c) {
		super(context, c, true);
		inflater = LayoutInflater.from(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		ViewHolder holder = (ViewHolder) view.getTag();
		if (holder == null) {
			holder = new ViewHolder();
			holder.productNameTextView = (TextView) view
					.findViewById(R.id.product_name);
			holder.productPriceTextView = (TextView) view
					.findViewById(R.id.product_price);
			holder.productImageView = (ImageView) view
					.findViewById(R.id.product_image);
			holder.colProdName = cursor.getColumnIndex(Product.COL_NAME);
			holder.colProdPrice = cursor.getColumnIndex(Product.COL_PRICE);
			holder.colProductId = cursor.getColumnIndex(Product.COL_ID);
			view.setTag(holder);
		}

		ProductImage productImage = ProductImageDaoWrapper.getInstance()
				.getMainProductImage(cursor.getInt(holder.colProductId));

		holder.productNameTextView
				.setText(cursor.getString(holder.colProdName));
		holder.productPriceTextView.setText(cursor
				.getString(holder.colProdPrice));
		if (productImage != null)
			ImageLoader.getInstance().displayImage(
					Uri.parse("file://" + productImage.getFilepath()).toString(),
					holder.productImageView);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup viewGrp) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.product_row, viewGrp, false);
	}

	static class ViewHolder {
		TextView productNameTextView, productPriceTextView;
		ImageView productImageView;
		int colProdName, colProdPrice, colProductId;
	}
}
