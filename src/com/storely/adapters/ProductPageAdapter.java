/**
 * 
 */
package com.storely.adapters;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.actionbarsherlock.app.ActionBar;
import com.storely.activities.ViewProducts;
import com.storely.entities.dao.DAOFacade;
import com.storely.entities.dao.ProductDaoWrapper;
import com.storely.entities.stores.Product;
import com.storely.fragments.ViewProduct;
import com.storely.shared.SharedProducts;

public class ProductPageAdapter extends
		CursorFragmentStatePagerAdapter<ViewProduct> implements
		OnPageChangeListener {

	private SparseArray<ViewProduct> positionAndFragments = new SparseArray<ViewProduct>();
	private int currPagerPosition;
	private ActionBar mBar;
	private ColumnIndexHolder colIndexHolder;

	public ProductPageAdapter(FragmentManager fm, Cursor cursor, ActionBar mBar) {
		super(fm, ViewProduct.class, cursor);
		this.mBar = mBar;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		super.destroyItem(container, position, object);
		removeItem(position);
	}

	@Override
	public ViewProduct getItem(int position) {

		ViewProduct viewProduct = super.getItem(position);


		if (colIndexHolder == null) {
			colIndexHolder = new ColumnIndexHolder();
			colIndexHolder.indexColId = cursor.getColumnIndex(Product.COL_ID);
		}
		
		Product product = ProductDaoWrapper.getInstance().getProduct(cursor.getInt(colIndexHolder.indexColId));

		Bundle args = new Bundle();
		args.putInt(ViewProducts.KEY_REQ_PROD_POS, position);
		viewProduct.setArguments(args);

		SharedProducts.put(position, product);
		positionAndFragments.put(position, viewProduct);

		return viewProduct;
	}

	public int getProductId(int position) {
		if (SharedProducts.get(position) != null)
			return SharedProducts.get(position).getId();
		return -1;
	}

	public ViewProduct getFragment(int position) {
		return positionAndFragments.get(position);
	}

	public void removeItem(int position) {
		SharedProducts.remove(position);
		positionAndFragments.remove(position);
	}

	@Override
	public int getItemPosition(Object object) {
		int position = positionAndFragments.keyAt(positionAndFragments
				.indexOfValue((ViewProduct) object));
		boolean exists = productExists(position);
		if (!exists)
			return POSITION_NONE;
		else {
			ProductDaoWrapper.getInstance().refresh(
					SharedProducts.get(position));
			positionAndFragments.get(position).refresh();
			return POSITION_UNCHANGED;
		}
	}

	public int getCurrPagerPosition() {
		return currPagerPosition;
	}

	public int getCurrProductId() {
		Product product = SharedProducts.get(currPagerPosition);
		if (product != null) 
			return product.getId();
		return -1;
	}

	public Product getCurrProduct() {
		return SharedProducts.get(currPagerPosition);
	}

	public boolean productExists(int position) {
		return (DAOFacade.productExists(SharedProducts.get(position)
				.getId()));
	}

	// OnPageChangeListener interface implementation methods

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int position, float arg1, int arg2) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPageSelected(int position) {
		// TODO Auto-generated method stub
		currPagerPosition = position;
		if (SharedProducts.get(position) != null)
		{
			mBar.setTitle(SharedProducts.get(position).getName());
		}
	}

	static class ColumnIndexHolder {
		int indexColId;
	}

}
