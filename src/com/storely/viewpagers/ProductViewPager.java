/**
 * 
 */
package com.storely.viewpagers;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class ProductViewPager extends ViewPager{

	public ProductViewPager(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public ProductViewPager(Context context,AttributeSet attrs) {
		super(context,attrs);
		// TODO Auto-generated constructor stub
	}

	
	// Have taken the code from Stackoverflow 
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        return super.canScroll(v, checkV, dx, x, y) || (checkV && customCanScroll(v));
    }
    protected boolean customCanScroll(View v) {
        if (v instanceof HorizontalScrollView) {
            View hsvChild = ((HorizontalScrollView) v).getChildAt(0);
            if (hsvChild.getWidth() > v.getWidth())
                return true;
        }
        return false;
   }
}
