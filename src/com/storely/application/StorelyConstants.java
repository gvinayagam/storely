/**
 * 
 */
package com.storely.application;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class StorelyConstants {

	public static final String STORE_URL = "http://www.storely.com";
	public static final String APP_PHOTOS_DIR = "Storely";
	public static final String CONTENT_URI_PREFIX = "content://com.storely.contentprovider/";
	
}
