/**
 * 
 */
package com.storely.loader;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.storely.entities.dao.ProductDaoWrapper;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class ProductCursorLoader extends CursorLoader{
	
	final ForceLoadContentObserver mObserver;

	public ProductCursorLoader(Context context) {
		super(context);
		mObserver = new ForceLoadContentObserver();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Cursor loadInBackground() {
		// TODO Auto-generated method stub
		Cursor cursor = ProductDaoWrapper.getInstance().getCursor();
		
		if(cursor != null)
		{
			cursor.getCount();
			cursor.registerContentObserver(mObserver);
		}
		
		return cursor;
	}

}
