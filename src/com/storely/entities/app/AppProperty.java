/**
 * 
 */
package com.storely.entities.app;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
@DatabaseTable(tableName = "appproperties")
public class AppProperty {
	
	public static final String COL_KEY = "key";
	public static final String COL_VALUe = "value";
	
	@DatabaseField
	private String key;
	
	@DatabaseField
	private String value;
	
	public AppProperty()
	{
		// Required by ormlite framework
	}

	public AppProperty(String key, String value)
	{
		this.key = key;
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
