/**
 * 
 */
package com.storely.entities.stores;

import com.j256.ormlite.field.DatabaseField;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class UpdatableEntity {

	public final String COL_UPDATE_COUNTER = "updatecounter";
	
	@DatabaseField
	private int updatecounter;

	/**
	 * @return the updatecounter
	 */
	public int getUpdatecounter() {
		return updatecounter;
	}

	/**
	 * @param updatecounter the updatecounter to set
	 */
	public void setUpdatecounter(int updatecounter) {
		this.updatecounter = updatecounter;
	}
	
	
}
