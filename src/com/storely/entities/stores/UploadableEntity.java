/**
 * 
 */
package com.storely.entities.stores;

import com.j256.ormlite.field.DatabaseField;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class UploadableEntity{
	
	public static final String COL_UPDATE_COUNTER = "updatecounter";
	public static final String COL_MODIFIED_TS = "modifiedts";
	public static final String COL_IS_DELETED = "deleted";
	public static final String COL_CREATED_TS = "createdts";
	
	@DatabaseField
	private long createdts;
	
	@DatabaseField
	private long modifiedts;
	
	@DatabaseField
	private int updatecounter;
	
	@DatabaseField
	private boolean deleted;
	
	public UploadableEntity()
	{
		// Used by ormlite framework
	}
	
	public long getModifiedts() {
		return modifiedts;
	}

	public void setModifiedts(long modifiedts) {
		this.modifiedts = modifiedts;
	}

	public int getUpdatecounter() {
		return updatecounter;
	}

	public void setUpdatecounter(int updatecounter) {
		this.updatecounter = updatecounter;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public long getCreatedts() {
		return createdts;
	}

	public void setCreatedts(long createdts) {
		this.createdts = createdts;
	}
}
