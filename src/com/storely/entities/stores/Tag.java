/**
 * 
 */
package com.storely.entities.stores;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
@DatabaseTable(tableName = "tags")
public class Tag extends UploadableEntity{

	public static final String COL_TAG = "tag";
	public static final String COL_ID = "_id";
	public static final String COL_PROD_ID = "product_id";
	
	@DatabaseField
	private String tag;
	
	@DatabaseField(columnName = "_id", generatedId=true)
	private int id;
	
	@DatabaseField(canBeNull = false, foreign = true)
	private Product product;

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
