/**
 * 
 */
package com.storely.entities.stores;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Gomathivinayagam Muthuvinayagam
 * 
 */

@DatabaseTable(tableName = "users")
public class User extends UploadableEntity {
	
	public static final String COL_MAILID = "mailid";
	public static final String COL_NAME = "name";
	public static final String COL_MOBILENO = "mobileno";

	@DatabaseField(columnName = "_id", canBeNull = false, id=true)
	private String mailid;

	@DatabaseField
	private String name;

	@DatabaseField
	private String mobileno;
	
	public User()
	{
		// No argument constructor required by OrmLite framework
	}

	public String getMailid() {
		return mailid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
}
