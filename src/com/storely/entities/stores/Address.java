/**
 * 
 */
package com.storely.entities.stores;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Gomathivinayagam Muthuvinayagam
 * 
 */

@DatabaseTable(tableName = "addresses")
public class Address extends UploadableEntity{
	
	public static final String COL_ADDRESS = "address";
	public static final String COL_ZIPCODE = "zipcode";
	public static final String COL_STATE = "state";
	public static final String COL_COUNTRY = "country";

	@DatabaseField
	private String address;

	@DatabaseField
	private String zipcode;

	@DatabaseField
	private String state;

	@DatabaseField
	private String country;

	public Address() {

	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
