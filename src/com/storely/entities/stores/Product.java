/**
 * 
 */
package com.storely.entities.stores;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Gomathivinayagam Muthuvinayagam
 * 
 */

@DatabaseTable(tableName = "products")
public class Product extends UploadableEntity{

	public static final String COL_ID = "_id";
	public static final String COL_NAME = "name";
	public static final String COL_PRICE = "price";
	public static final String COL_DES = "desc";
	
	@DatabaseField(columnName = "_id", generatedId=true)
	private int id;

	@DatabaseField
	private String name;

	@DatabaseField
	private float price;
	
	@DatabaseField
	private String desc;

	public Product() {
		// Used by ormlite framework
	}
	
	public Product(String name)
	{
		this.name = name;
	}
	
	public Product(String name, float price, String desc)
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
	}
	
	public Product(int id,String name, float price, String desc)
	{
		this.id = id;
		this.name = name;
		this.price = price;
		this.desc = desc;
	}
	
	public Product(int id)
	{
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean equals(Product another)
	{
		if(this.price != another.price)
			return false;
		if(this.desc != null && another.desc != null && !(this.desc.equals(another.desc)) )
			return false;
		if(this.name != null && another.name != null && !(this.name.equals(another.name)) )
			return false;
		return true;
	}
}
