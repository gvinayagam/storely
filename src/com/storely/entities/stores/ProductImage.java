/**
 * 
 */
package com.storely.entities.stores;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
@DatabaseTable(tableName="productimages")
public class ProductImage extends UploadableEntity {
	
	public static final String COL_ID = "_id";
	public static final String COL_PRODUCT_ID = "product_id";
	public static final String COL_IS_MAIN_IMAGE = "isMain";
	public static final String COL_FILE_PATH = "filepath";
	
	@DatabaseField(columnName = "_id", generatedId=true)
	private int id;
	
	@DatabaseField
	private boolean isMain;
	
	@DatabaseField
	private String filepath;

	@DatabaseField(canBeNull = false, foreign = true)
	private Product product;
	
	public ProductImage()
	{
		// Used by Ormlite framework
	}
	
	public ProductImage(String filepath, boolean isMain, Product product )
	{
		this.filepath = filepath;
		this.isMain = isMain;
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public boolean isMain() {
		return isMain;
	}

	public void setMain(boolean isMain) {
		this.isMain = isMain;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
}
