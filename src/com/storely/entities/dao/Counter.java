/**
 * 
 */
package com.storely.entities.dao;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class Counter {
	
	private AtomicInteger updateCounter = new AtomicInteger();
	
	public Counter(int cntr)
	{
		updateCounter.set(cntr);
	}
	
	// Used by all DAO classes, and inserted into all tables during insert, delete and update 
	// operations
	public int getCounter()
	{
		return updateCounter.get();
	}
	
	public int getNextCounter()
	{
		return updateCounter.incrementAndGet();
	}

}
