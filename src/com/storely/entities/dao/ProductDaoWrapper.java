/**
 * 
 */
package com.storely.entities.dao;

import java.sql.SQLException;

import android.database.Cursor;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.storely.entities.stores.Product;

/**
 * @author Gomathivinayagam Muthuvinayagam
 * 
 */
public class ProductDaoWrapper extends UploadableDaoWrapper<Product, Integer> {

	private static ProductDaoWrapper productDaoWrapper;

	static {
		productDaoWrapper = new ProductDaoWrapper();
	}

	public static ProductDaoWrapper getInstance() {
		return productDaoWrapper;
	}

	private ProductDaoWrapper() {
		super(Product.class);
	}
	
	public boolean isNewlyCreated(int id) {
		Product product = getProduct(id);
		if (product != null)
			return product.getModifiedts() == 0;
		return false;
	}
	
	public static boolean isNewlyCreated(Product product) {
		if (product != null)
			return product.getModifiedts() == 0;
		return false; 
	}


	public Product getProduct(int id) {
		try {
			return dao.queryForId(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public Cursor getCursor() {
		QueryBuilder<Product, Integer> prodQB = dao.queryBuilder();
		try {
			prodQB.orderBy(Product.COL_CREATED_TS, false).where()
					.eq(Product.COL_IS_DELETED, false);
			return super.getCursor(prodQB.prepare());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*// This function is used to get the product name of nth row from the select
	// query which is used in getCursor() function
	public String getProductName(int n) {
		QueryBuilder<Product, Integer> prodQB = dao.queryBuilder();
		try {
			prodQB.orderBy(Product.COL_CREATED_TS, false).where()
					.ne(Product.COL_IS_DELETED, true);
			prodQB.offset(n).limit(1);
			return prodQB.query().get(0).getName();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Product getProductFrom(int pos) {
		QueryBuilder<Product, Integer> prodQB = dao.queryBuilder();
		try {
			prodQB.orderBy(Product.COL_CREATED_TS, false).where()
					.ne(Product.COL_IS_DELETED, true);
			prodQB.offset(pos).limit(1);
			return prodQB.query().get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}*/

	public static String getProductName(Cursor cursor) {
		return cursor.getString(cursor.getColumnIndex(Product.COL_NAME));
	}
	
	public void deleteProduct(int productId)
	{
		UpdateBuilder<Product, Integer> prodUB = dao.updateBuilder();
		try {
			prodUB.updateColumnValue(Product.COL_IS_DELETED, true).where().eq(Product.COL_ID, productId);
			prodUB.update();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Product createEmptyProduct()
	{
		Product product = new Product();
		if(super.create(product))
			return product;
		else
			return null;
	}
}
