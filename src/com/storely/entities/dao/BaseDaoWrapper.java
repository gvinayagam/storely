/**
 * 
 */
package com.storely.entities.dao;

import java.sql.SQLException;
import java.util.List;

import android.database.Cursor;
import android.net.Uri;

import com.j256.ormlite.android.AndroidDatabaseResults;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.storely.application.StorelyConstants;
import com.storely.database.DatabaseHelper;
import com.storely.database.DatabaseManager;

/**
 * @author Gomathivinayagam Muthuvinayagam
 * @param <T>
 * @param <ID>
 * 
 */
public class BaseDaoWrapper<T, ID> {
	protected static DatabaseHelper databaseHelper = DatabaseManager
			.getInstance().getHelper();
	protected Dao<T, ID> dao;
	protected Uri uri;
	private QueryBuilder<T,ID> qb;
	private UpdateBuilder<T, ID> ub;

	protected BaseDaoWrapper(Class<T> entityType) {
		try {
			dao = databaseHelper.getDao(entityType);
			uri = Uri.parse(StorelyConstants.CONTENT_URI_PREFIX
					+ entityType.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected QueryBuilder<T,ID> getQueryBuilder()
	{
		if(qb == null)
			qb = dao.queryBuilder();
		return qb;
	}
	
	protected UpdateBuilder<T,ID> getUpdateBuilder()
	{
		if(ub == null)
			ub = dao.updateBuilder();
		return ub;
	}

	protected boolean create(T data) {
		boolean result = true;
		try {
			result = (dao.create(data) == 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		}
		if (result)
			notifyCursorChanges();
		return result;
	}

	protected boolean update(T data) {
		boolean result = true;
		try {
			result = (dao.update(data) == 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		}
		if (result)
			notifyCursorChanges();
		return result;
	}

	public void refresh(T data) {
		try {
			dao.refresh(data);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected T getFirst() {
		try {
			QueryBuilder<T, ID> queryBuilder = getQueryBuilder();
			queryBuilder.queryForFirst();
			PreparedQuery<T> firstStoreQuery = queryBuilder.prepare();
			return dao.query(firstStoreQuery).get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected List<T> getAll() {
		try {
			return dao.queryForAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	protected long getSize() {
		try {
			return dao.countOf();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

	protected Cursor getCursor() {
		Cursor cursor = null;

		CloseableIterator<T> iterator = dao.iterator();
		cursor = ((AndroidDatabaseResults) iterator.getRawResults())
				.getRawCursor();
		cursor.setNotificationUri(DatabaseManager.getInstance()
				.getContentResolver(), uri);

		return cursor;
	}

	protected Cursor getCursor(PreparedQuery<T> query) {
		Cursor cursor = null;
		try {
			CloseableIterator<T> iterator = dao.iterator(query);
			cursor = ((AndroidDatabaseResults) iterator.getRawResults())
					.getRawCursor();
			registerCursorNotification(cursor);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cursor;
	}

	protected void registerCursorNotification(Cursor cursor) {
		if (cursor != null) {
			cursor.setNotificationUri(DatabaseManager.getInstance()
					.getContentResolver(), uri);
		}
	}

	protected void notifyCursorChanges() {
		DatabaseManager.getInstance().getContentResolver()
				.notifyChange(uri, null);
	}
}
