/**
 * 
 */
package com.storely.entities.dao;

import com.storely.entities.stores.Address;

/**
 * @author Gomathivinayagam Muthuvinayagam
 *
 */
public class AddressDaoWrapper extends UploadableDaoWrapper<Address,Void> {

	private static AddressDaoWrapper addressDaoWrapper;
	
	static 
	{ 
		addressDaoWrapper = new AddressDaoWrapper();
	}
	
	public static AddressDaoWrapper getInstance()
	{
		return addressDaoWrapper;
	}
	
	private AddressDaoWrapper()
	{
		super(Address.class);
	}
}
