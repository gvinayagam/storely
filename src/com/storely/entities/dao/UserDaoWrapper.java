/**
 * 
 */
package com.storely.entities.dao;

import com.storely.entities.stores.User;

/**
 * @author Gomathivinayagam Muthuvinayagam
 *
 */
public class UserDaoWrapper extends UploadableDaoWrapper<User,Integer> {

	private static UserDaoWrapper userDaoWrapper;
	
	static 
	{ 
		userDaoWrapper = new UserDaoWrapper();
	}
	
	public static UserDaoWrapper getInstance()
	{
		return userDaoWrapper;
	}
	
	private UserDaoWrapper()
	{
		super(User.class);
	}
	
}
