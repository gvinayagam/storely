/**
 * 
 */
package com.storely.entities.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.storely.entities.app.AppProperty;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class AppPropertyDaoWrapper extends BaseDaoWrapper<AppProperty,Void>{
	
	private static AppPropertyDaoWrapper appPropertyDaoWrapper;
	private static Dao<AppProperty,Void> appPropertyDao;
	
	static 
	{ 
		appPropertyDaoWrapper = new AppPropertyDaoWrapper();
		appPropertyDao = appPropertyDaoWrapper.dao;
	}
	
	public static AppPropertyDaoWrapper getInstance()
	{
		return appPropertyDaoWrapper;
	}

	private AppPropertyDaoWrapper()
	{
		super(AppProperty.class);
	}
	
	public static boolean exists(String key)
	{
		try {
			System.out.println(appPropertyDao.queryForEq(AppProperty.COL_KEY, key));
			return appPropertyDao.queryForEq(AppProperty.COL_KEY, key).size() > 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean put(String key,String value)
	{
		AppProperty appProperty = new AppProperty(key,value);
		try
		{
			if(exists(key))
			{
				appPropertyDao.update(appProperty);
			}
			else
			{
				appPropertyDao.create(appProperty);
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static String get(String key)
	{
		try
		{
			if(exists(key))
				return appPropertyDao.queryForEq(AppProperty.COL_KEY, key).get(0).getValue();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
