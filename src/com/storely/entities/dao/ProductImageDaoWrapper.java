/**
 * 
 */
package com.storely.entities.dao;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.storely.entities.stores.ProductImage;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 * 
 */
public class ProductImageDaoWrapper extends
		UploadableDaoWrapper<ProductImage, Integer> {

	private static ProductImageDaoWrapper pImageWrapper;

	static {
		pImageWrapper = new ProductImageDaoWrapper();
	}

	public static ProductImageDaoWrapper getInstance() {
		return pImageWrapper;
	}

	private ProductImageDaoWrapper() {
		super(ProductImage.class);
	}

	public ProductImage getMainProductImage(long productId) {

		QueryBuilder<ProductImage, Integer> prodQB = getQueryBuilder();
		try {
			prodQB.where().eq(ProductImage.COL_PRODUCT_ID, productId).and()
					.eq(ProductImage.COL_IS_MAIN_IMAGE, true);
			return dao.query(prodQB.prepare()).get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean updateMainProductImage(int productId)
	{
		UpdateBuilder<ProductImage, Integer> prodUB = getUpdateBuilder();
		try {
			prodUB.updateColumnValue(ProductImage.COL_IS_MAIN_IMAGE, true).where().eq(ProductImage.COL_PRODUCT_ID, productId);
			prodUB.update();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public List<ProductImage> getProductImages(long productId) {
		QueryBuilder<ProductImage, Integer> prodQB = getQueryBuilder();
		try {
			prodQB.orderBy(ProductImage.COL_MODIFIED_TS, false).where()
					.eq(ProductImage.COL_PRODUCT_ID, productId);
			return dao.query(prodQB.prepare());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void deleteProductImages(int productId)
	{
		UpdateBuilder<ProductImage, Integer> prodUB = getUpdateBuilder();
		try {
			prodUB.updateColumnValue(ProductImage.COL_IS_DELETED, true).where().eq(ProductImage.COL_PRODUCT_ID, productId);
			prodUB.update();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
