/**
 * 
 */
package com.storely.entities.dao;

import com.storely.entities.stores.Product;
import com.storely.entities.stores.ProductImage;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class DAOFacade {

	public static void addProductImage(ProductImage pImg)
	{
		ProductImageDaoWrapper.getInstance().create(pImg);
	}
	
	public static void addAddress()
	{
		
	}
	
	public static void updateAddress()
	{
		
	}
	
	public static void addProduct(Product p)
	{
		ProductDaoWrapper.getInstance().create(p);
	}
	
	public static void updateProduct(Product p)
	{
		ProductDaoWrapper.getInstance().update(p);
	}
	
	public static void addUser()
	{
		
	}
	
	public static void deleteProduct(int productId)
	{
		ProductDaoWrapper.getInstance().deleteProduct(productId);
		ProductImageDaoWrapper.getInstance().deleteProductImages(productId);
		ProductDaoWrapper.getInstance().notifyCursorChanges();
	}
	
	public static boolean productExists(int productId)
	{
		Product product = ProductDaoWrapper.getInstance().getProduct(productId);
		if(product == null)
			return false;
		return !product.isDeleted();
	}
}
