/**
 * 
 */
package com.storely.entities.dao;

import com.storely.entities.stores.Tag;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 *
 */
public class TagDaoWrapper extends UploadableDaoWrapper<Tag,Integer>{
	
	private static TagDaoWrapper tagDaoWrapper;
	
	static 
	{ 
		tagDaoWrapper = new TagDaoWrapper();
	}
	
	public static TagDaoWrapper getInstance()
	{
		return tagDaoWrapper;
	}

	protected TagDaoWrapper() {
		super(Tag.class);
		// TODO Auto-generated constructor stub
	}

}
