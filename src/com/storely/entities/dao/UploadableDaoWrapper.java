/**
 * 
 */
package com.storely.entities.dao;

import java.sql.SQLException;
import java.util.List;

import com.storely.entities.stores.UploadableEntity;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 * @param <T>
 * @param <ID>
 * 
 */
public abstract class UploadableDaoWrapper<T, ID> extends BaseDaoWrapper<T, ID> {

	protected Counter cntr;
	private UploadableEntity uploadableEntity;

	protected UploadableDaoWrapper(Class<T> entityType) {
		super(entityType);
		cntr = new Counter(getLastUsedCounter());
	}

	private int getLastUsedCounter() {
		try {
			uploadableEntity = (UploadableEntity) dao.queryBuilder()
					.selectColumns(UploadableEntity.COL_UPDATE_COUNTER)
					.orderBy(UploadableEntity.COL_UPDATE_COUNTER, false)
					.queryForFirst();
			return ((uploadableEntity != null) ? uploadableEntity.getUpdatecounter() : -1);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return -1;
	}

	public int getCurrCounter() {
		return cntr.getCounter();
	}
	
	protected int getNextCounter() {
		return cntr.getNextCounter();
	}

	public List<T> getUpdatesToUpload(int prevCntr) {
		// TODO Auto-generated method stub
		try {
			return dao.queryBuilder().where()
					.gt(UploadableEntity.COL_UPDATE_COUNTER, prevCntr)
					.query();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean create(T data)
	{
		((UploadableEntity) data).setUpdatecounter(getNextCounter());
		((UploadableEntity) data).setCreatedts(System.currentTimeMillis());
		return super.create(data);
	}
	
	public boolean update(T data)
	{
		((UploadableEntity) data).setUpdatecounter(getNextCounter());
		((UploadableEntity) data).setModifiedts(System.currentTimeMillis());
		return super.update(data);
	}
	
}
