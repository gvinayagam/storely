/**
 * 
 */
package com.storely.fragments;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.storely.R;
import com.storely.activities.ViewProducts;
import com.storely.entities.dao.ProductImageDaoWrapper;
import com.storely.entities.stores.ProductImage;
import com.storely.shared.SharedProducts;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 * 
 */
public class ViewProduct extends SherlockFragment {

	private ImageView mainProductImageView;
	private TextView productTextView, productPriceView;
	private LinearLayout subProductsView;
	private ProgressBar spinner;
	private int currProductPos;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_product_page, container,
				false);
		subProductsView = (LinearLayout) view
				.findViewById(R.id.subproductimages);
		mainProductImageView = (ImageView) view
				.findViewById(R.id.main_product_image);
		productTextView = (TextView) view.findViewById(R.id.product_text);
		productPriceView = (TextView) view.findViewById(R.id.product_price);
		spinner = (ProgressBar) view.findViewById(R.id.loading);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		currProductPos = getArguments().getInt(ViewProducts.KEY_REQ_PROD_POS);

		List<ProductImage> productImages = ProductImageDaoWrapper.getInstance()
				.getProductImages(SharedProducts.get(currProductPos).getId());

		if (productImages != null && productImages.size() > 0) {
			ImageLoader.getInstance().displayImage(
					Uri.parse("file://" + productImages.get(0).getFilepath())
							.toString(), mainProductImageView,
					new ProductImageLoadingListener());

			for (ProductImage productImage : productImages)
				subProductsView.addView(createSubProductView(productImage
						.getFilepath()));
		}

		refresh();
	}

	private View createSubProductView(String subProductFile) {

		LayoutInflater inflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View subProductView = inflater
				.inflate(R.layout.sub_product_image, null);

		ImageView subProductImageView = (ImageView) subProductView
				.findViewById(R.id.sub_product_image);
		ImageLoader.getInstance().displayImage(
				Uri.parse("file://" + subProductFile).toString(),
				subProductImageView);

		return subProductView;
	}

	public void onProductImageClick(View view) {
		Toast.makeText(getActivity(), "Button has been clicked",
				Toast.LENGTH_SHORT).show();
	}

	private class ProductImageLoadingListener implements ImageLoadingListener {

		@Override
		public void onLoadingStarted(String imageUri, View view) {
			// TODO Auto-generated method stub
			spinner.setVisibility(View.VISIBLE);
		}

		@Override
		public void onLoadingFailed(String imageUri, View view,
				FailReason failReason) {
			// TODO Auto-generated method stub
			spinner.setVisibility(View.GONE);
		}

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			// TODO Auto-generated method stub
			spinner.setVisibility(View.GONE);
		}

		@Override
		public void onLoadingCancelled(String imageUri, View view) {
			// TODO Auto-generated method stub
			spinner.setVisibility(View.GONE);
		}

	}

	// Method is called to update the view on product details changes
	public void refresh() {
		// TODO Auto-generated method stub
		refreshText();
	}
	
	private void refreshText()
	{
		productTextView.setText(SharedProducts.get(currProductPos).getName());
		productPriceView.setText(Float.toString(SharedProducts.get(currProductPos).getPrice()));
	}

}
