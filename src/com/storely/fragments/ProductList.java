/**
 * 
 */
package com.storely.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.storely.R;
import com.storely.adapters.ProductListAdapter;
import com.storely.loader.ProductCursorLoader;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 * 
 */
public class ProductList extends SherlockListFragment implements OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

	ProductListFragmentListener plfListener;
	ProductListAdapter pdtCursorAdapter;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			plfListener = (ProductListFragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement " + ProductListFragmentListener.class);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		pdtCursorAdapter = ProductListAdapter.getInstance(getActivity());
		setListAdapter(pdtCursorAdapter);
		
		getListView().setOnItemClickListener(this);
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.product_list, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.actionbar_add_but) {
			plfListener.onAddProduct();
		}
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		// TODO Auto-generated method stub
		plfListener.onProductSelected(position);
	}

	// Loader callback implementation methods
	
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		return new ProductCursorLoader(getActivity());
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		// TODO Auto-generated method stub
		pdtCursorAdapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		pdtCursorAdapter.swapCursor(null);
	}
	
	
	// Activity which hosts this fragment should implement the following interface
	
	public interface ProductListFragmentListener {
		public void onAddProduct();
		public void onProductSelected(int position);
	}

}
