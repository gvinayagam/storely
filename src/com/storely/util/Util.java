/**
 * 
 */
package com.storely.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.storely.application.StorelyConstants;
import com.storely.preferences.StorelyPreferences;

/**
 * @author Gomathivinayagam Muthuvinayagam
 * 
 */
public class Util extends StorelyConstants {

	private static final String HTTPS = "https://";
	private static final String HTTP = "http://";
	private static final String JPEG_FILE_SUFFIX = ".jpeg";

	public static void openBrowser(final Context context, String url) {

		if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
			url = HTTP + url;
		}

		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(Intent.createChooser(intent, "Choose browser"));
	}

	public static void launchStorelyOnBrowser(final Context context) {
		openBrowser(
				context,
				StorelyConstants.STORE_URL
						+ "/"
						+ StorelyPreferences
								.getPropertyAsString(StorelyPreferences.KEY_STORE_ID));
	}

	public static File getAppPhotosDir() {
		File albumDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				APP_PHOTOS_DIR);
		if (!albumDir.exists())
			albumDir.mkdirs();
		return albumDir;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getTimestamp() {
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		return timeStamp;
	}

	/* Checks if external storage is available for read and write */
	public static boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	// Creates a file with jpeg extension on the album directory for this
	// application
	public static File createImageFile(String fileName) throws IOException {
		// Create an image file name
		String imageFileName = fileName + "_";
		File image = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX,
				getAppPhotosDir());
		return image;
	}

}
