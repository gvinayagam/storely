package com.storely.activities;

import com.storely.R;
import com.storely.R.layout;
import com.storely.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class ManageTags extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_tags);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_tags, menu);
		return true;
	}

}
