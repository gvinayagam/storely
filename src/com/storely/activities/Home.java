package com.storely.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.storely.R;
import com.storely.fragments.NewsFeed;
import com.storely.fragments.ProductList;
import com.storely.fragments.ProductList.ProductListFragmentListener;
import com.storely.preferences.StorelyPreferences;
import com.storely.util.Util;

public class Home extends SherlockFragmentActivity implements
		ProductListFragmentListener {

	private ActionBar actionBar;
	private NewsFeed newsFeed;
	private ProductList productList;

	// App specific activity variables
	private final CharSequence[] MENU_LIST = { "My store on Web", "My Account",
			"My Store", "Settings" };
	private final String[] TAB_HEADINGS = { "Home", "My items" };

	// Lifecycle method overrides
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		createTabs();
		initAppViewdata();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getSupportMenuInflater();
		menuInflater.inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.actionbar_menu_but) {
			showGlobalMenu();
		} else if (item.getItemId() == R.id.homeAsUp) {
			Intent homeIntent = new Intent(getApplicationContext(), Home.class);
			homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(homeIntent);
		}
		return super.onOptionsItemSelected(item);
	}

	// Other methods

	private void createTabs() {
		for (String tabHeading : TAB_HEADINGS) {
			Tab newTab = actionBar.newTab();
			newTab.setText(tabHeading);
			newTab.setTabListener(new HomeTabsListener());
			actionBar.addTab(newTab);
		}
	}

	private void showGlobalMenu() {
		// TODO Auto-generated method stub
		AlertDialog.Builder listBuilder = new AlertDialog.Builder(this);
		listBuilder.setItems(MENU_LIST, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0:
					Util.launchStorelyOnBrowser(Home.this);
					break;
				}

			}
		});
		AlertDialog alertList = listBuilder.create();
		alertList.setCanceledOnTouchOutside(true);
		alertList.show();
	}

	private void initAppViewdata() {
		actionBar.setTitle(StorelyPreferences
				.getPropertyAsString(StorelyPreferences.KEY_STORE_NAME));
	}

	// Interface implementation methods for ProductListFragmentListener

	@Override
	public void onAddProduct() {
		Intent createProductIntent = new Intent(getApplicationContext(),
				CreateProduct.class);
		startActivity(createProductIntent);
	}

	@Override
	public void onProductSelected(int position) {
		// TODO Auto-generated method stub
		Intent viewProductIntent = new Intent(getApplicationContext(),
				ViewProducts.class);
		viewProductIntent.putExtra(ViewProducts.KEY_REQ_PROD_POS, position);
		viewProductIntent.putExtra(ViewProducts.KEY_REQ_TYPE,
				ViewProducts.ReqType.VIEW_MODE);
		startActivity(viewProductIntent);
	}

	// Tabs listener implementation

	private class HomeTabsListener implements TabListener {
		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			if (tab.getPosition() == 0) {
				if (newsFeed == null)
					newsFeed = new NewsFeed();
				ft.replace(android.R.id.content, newsFeed);
			} else if (tab.getPosition() == 1) {
				if (productList == null)
					productList = new ProductList();
				ft.replace(android.R.id.content, productList);
			}
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
		}
	}

}
