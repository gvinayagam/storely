/**
 * 
 */
package com.storely.activities;

import java.io.File;
import java.io.IOException;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.storely.entities.dao.DAOFacade;
import com.storely.entities.dao.ProductDaoWrapper;
import com.storely.entities.stores.Product;
import com.storely.entities.stores.ProductImage;
import com.storely.util.Util;

/**
 * @author "Gomathivinayagam Muthuvinayagam"
 * 
 */
public class CreateProduct extends SherlockFragmentActivity {

	private static final int CAMERA_PIC_REQUEST = 100;
	private static final String ERR_MSG_STORAGE_NA = "External storage is in use. Disconnect cable from Phone, and try again.";
	private static final String ERR_MSG_CAMERA_NA = "Oops! Capturing images not supported by your phone";
	
	private AddProductRequest addProductRequest = new AddProductRequest();

	// Lifecycle method overrides
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		createProduct();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {
			if (requestCode == CAMERA_PIC_REQUEST) {
				addProductRequest.product = ProductDaoWrapper.getInstance()
						.createEmptyProduct();
				addProductImgToDB(addProductRequest.productImagePath, true,
						addProductRequest.product);
				launchViewProductsOnEditMode(0);
			}
		} else if (resultCode == RESULT_CANCELED)
		{
			if (requestCode == CAMERA_PIC_REQUEST) {
				finish();
			}
		}
	}

	// Called when the user clicks takephoto button
	public void takePhoto(File imageFile) {

		try {
			Intent cameraIntent = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
					Uri.fromFile(imageFile));
			startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);

		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, ERR_MSG_CAMERA_NA, Toast.LENGTH_SHORT).show();
		}
	}

	private void addProductImgToDB(String imageFileName, boolean isMain,
			Product product) {
		ProductImage pImage = new ProductImage(imageFileName, isMain, product);
		DAOFacade.addProductImage(pImage);
	}

	public void launchViewProductsOnEditMode(int position) {
		Intent viewProductIntent = new Intent(getApplicationContext(),
				ViewProducts.class);
		viewProductIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		viewProductIntent.putExtra(ViewProducts.KEY_REQ_PROD_POS, position);
		viewProductIntent.putExtra(ViewProducts.KEY_REQ_TYPE,
				ViewProducts.ReqType.EDIT_MODE);
		startActivity(viewProductIntent);
		finish();
	}

	protected void createProduct() {
		// 1) Gets an image for the product from the user
		// 2) Launches an activity to get the details about the product
		// 3) On successful return from (1) & (2), launches an activity to
		// display the recently added product
		// 4) On failure of step (1) & (2), returns to the product list view
		if (!Util.isExternalStorageWritable()) {
			Toast.makeText(getApplicationContext(), ERR_MSG_STORAGE_NA,
					Toast.LENGTH_LONG).show();
			finish();
		}
		String imgName = Util.getTimestamp();
		File imageFile;
		try {
			imageFile = Util.createImageFile(imgName);
			addProductRequest.productImagePath = imageFile.getAbsolutePath();
			takePhoto(imageFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Class to store variables for the add product request

	private class AddProductRequest {
		private String productImagePath;
		private Product product;
	}

}
