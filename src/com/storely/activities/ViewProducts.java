package com.storely.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.storely.R;
import com.storely.adapters.ProductPageAdapter;
import com.storely.entities.dao.DAOFacade;
import com.storely.loader.ProductCursorLoader;

public class ViewProducts extends SherlockFragmentActivity implements
		LoaderCallbacks<Cursor> {

	public static final int GET_PRODUCT_DETAILS_REQ = 100;
	public static final String KEY_REQ_TYPE = "reqType";
	public static final String KEY_REQ_PROD_POS = "reqProductPos";

	// VIEW_MODE, just displays the product
	// EDIT_MODE, displays the product, and opens the details, and handles the updates on that
	public static enum ReqType {
		EDIT_MODE, VIEW_MODE
	}

	ViewPager productPager;
	ProductPageAdapter productPagerAdapter;
	int requestedProduct = -1;
	ReqType reqType;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_products);
		
		requestedProduct = getIntent().getIntExtra(KEY_REQ_PROD_POS, -1);
		reqType = (ReqType) getIntent().getSerializableExtra(KEY_REQ_TYPE);

		getSupportLoaderManager().initLoader(0, null, this);
		productPager = (ViewPager) findViewById(R.id.product_pager);
		productPagerAdapter = new ProductPageAdapter(
				getSupportFragmentManager(), null, getSupportActionBar());
		productPager.setAdapter(productPagerAdapter);
		productPager.setOnPageChangeListener(productPagerAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getSupportMenuInflater();
		menuInflater.inflate(R.menu.view_products, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.actionbar_add_but) {
			Intent createProductIntent = new Intent(getApplicationContext(), CreateProduct.class);
			startActivity(createProductIntent);
		}
		else if (item.getItemId() == R.id.actionbar_delete_but) {
			int currProductId = productPagerAdapter.getCurrProductId();
			requestedProduct = productPagerAdapter.getCurrPagerPosition();
			DAOFacade.deleteProduct(currProductId);
		} else if (item.getItemId() == R.id.actionbar_edit_but) {
			requestedProduct = productPagerAdapter.getCurrPagerPosition();
			Intent editProductIntent = new Intent(getApplicationContext(),
					ProductDetails.class);
			editProductIntent.putExtra(KEY_REQ_PROD_POS,
					productPagerAdapter.getCurrPagerPosition());
			startActivity(editProductIntent);
		}
		return true;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_CANCELED) {
			if (requestCode == GET_PRODUCT_DETAILS_REQ) {
				DAOFacade.deleteProduct(productPagerAdapter
						.getProductId(productPagerAdapter
								.getCurrPagerPosition()));
				super.onBackPressed();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	// Loader callback implementation methods

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		return new ProductCursorLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
		// TODO Auto-generated method stub
		productPagerAdapter.swapCursor(cursor);
		if (productPagerAdapter.getCount() == 0) {
			finish();
		}
		
		if (requestedProduct != -1) {
			productPager.setCurrentItem(requestedProduct, false);
			productPagerAdapter.onPageSelected(requestedProduct);
			requestedProduct = -1;
		}
		
		if (reqType != null && reqType == ReqType.EDIT_MODE) {
			reqType = null;
			requestedProduct = productPagerAdapter.getCurrPagerPosition();
			Intent editProductIntent = new Intent(getApplicationContext(),
					ProductDetails.class);
			editProductIntent.putExtra(KEY_REQ_PROD_POS,
					productPagerAdapter.getCurrPagerPosition());
			startActivityForResult(editProductIntent, GET_PRODUCT_DETAILS_REQ);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		productPagerAdapter.swapCursor(null);
	}

}
