package com.storely.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.storely.R;

public class Welcome extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
	}

	public void onClickStartNow(View view)
	{
		startHomeActivity();
	}
	
	public void startHomeActivity()
	{
		Intent homeIntent = new Intent(getApplicationContext(),	Home.class);
		startActivity(homeIntent);
		finish();
	}
	
}
