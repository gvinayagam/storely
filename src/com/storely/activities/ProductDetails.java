package com.storely.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.storely.R;
import com.storely.entities.dao.DAOFacade;
import com.storely.entities.stores.Product;
import com.storely.shared.SharedProducts;

public class ProductDetails extends SherlockActivity {

	private EditText name, price, desc;
	private Product product;

	private static final String TITLE_NEW_PRODUCT = "Add product";
	private static final String TITLE_EDIT_PRODUCT = "Edit product";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_details);

		price = (EditText) findViewById(R.id.text_product_price);
		desc = (EditText) findViewById(R.id.text_product_des);
		name = (EditText) findViewById(R.id.text_product_name);

		int position = getIntent().getExtras().getInt(ViewProducts.KEY_REQ_PROD_POS, -1);
		product = SharedProducts.get(position);

		if (product != null) {
			name.setText(product.getName());
			if (product.getPrice() != 0)
				price.setText(Float.toString(product.getPrice()));
			desc.setText(product.getDesc());
			
			if (product.getModifiedts() == 0) {
				setTitle(TITLE_NEW_PRODUCT);
			} else {
				setTitle(TITLE_EDIT_PRODUCT);
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent homeIntent = new Intent(getApplicationContext(), Home.class);
			homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(homeIntent);
			break;
		case R.id.actionbar_cancel:
			cancel();
			break;
		case R.id.actionbar_save:
			save();
			break;
		default:
			break;
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getSupportMenuInflater();
		menuInflater.inflate(R.menu.create_product, menu);
		return true;
	}

	private float getPrice() {
		return (price.getText().length() > 0) ? Float.valueOf(price.getText()
				.toString()) : 0;
	}

	private String getProductName() {
		return name.getText().toString();
	}

	private String getProductDesc() {
		return desc.getText().toString();
	}

	private boolean validateInput() {
		boolean valid = true;
		View requireFocus = null;

		name.setError(null);
		price.setError(null);

		if (TextUtils.isEmpty(name.getText().toString())) {
			name.setError(getString(R.string.error_field_required));
			requireFocus = name;
			valid = false;
		}
		if (TextUtils.isEmpty(price.getText().toString())
				|| (Float.valueOf(price.getText().toString()) == 0)) {
			price.setError(getString(R.string.error_field_required));
			requireFocus = name;
			valid = false;
		}

		if (!valid) {
			requireFocus.requestFocus();
		}
		return valid;
	}

	private void cancel() {
		respondResult(RESULT_CANCELED);
	}

	private void save() {
		if (validateInput()) {
			Product another = new Product(getProductName(), getPrice(), getProductDesc());
			// Let us update database only if there are changes to product details
			if(!product.equals(another))
			{
				product.setName(getProductName());
				product.setPrice(getPrice());
				product.setDesc(getProductDesc());
				
				DAOFacade.updateProduct(product);
				respondResult(RESULT_OK);
			}
			else
			{
				respondResult(RESULT_CANCELED);
			}
		}
	}

	private void respondResult(int returnCode) {
		Intent returnIntent = new Intent();
		setResult(returnCode, returnIntent);
		finish();
	}

}
