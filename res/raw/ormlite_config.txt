#
# generated on 2013/06/11 10:08:25
#
# --table-start--
dataClass=com.storely.entities.app.AppProperty
tableName=appproperties
# --table-fields-start--
# --field-start--
fieldName=key
# --field-end--
# --field-start--
fieldName=value
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.storely.entities.stores.Address
tableName=addresses
# --table-fields-start--
# --field-start--
fieldName=address
# --field-end--
# --field-start--
fieldName=zipcode
# --field-end--
# --field-start--
fieldName=state
# --field-end--
# --field-start--
fieldName=country
# --field-end--
# --field-start--
fieldName=createdts
# --field-end--
# --field-start--
fieldName=modifiedts
# --field-end--
# --field-start--
fieldName=updatecounter
# --field-end--
# --field-start--
fieldName=deleted
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.storely.entities.stores.Product
tableName=products
# --table-fields-start--
# --field-start--
fieldName=id
columnName=_id
generatedId=true
# --field-end--
# --field-start--
fieldName=name
# --field-end--
# --field-start--
fieldName=price
# --field-end--
# --field-start--
fieldName=desc
# --field-end--
# --field-start--
fieldName=createdts
# --field-end--
# --field-start--
fieldName=modifiedts
# --field-end--
# --field-start--
fieldName=updatecounter
# --field-end--
# --field-start--
fieldName=deleted
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.storely.entities.stores.ProductImage
tableName=productimages
# --table-fields-start--
# --field-start--
fieldName=id
columnName=_id
generatedId=true
# --field-end--
# --field-start--
fieldName=isMain
# --field-end--
# --field-start--
fieldName=filepath
# --field-end--
# --field-start--
fieldName=product
canBeNull=false
foreign=true
# --field-end--
# --field-start--
fieldName=createdts
# --field-end--
# --field-start--
fieldName=modifiedts
# --field-end--
# --field-start--
fieldName=updatecounter
# --field-end--
# --field-start--
fieldName=deleted
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.storely.entities.stores.Tag
tableName=tags
# --table-fields-start--
# --field-start--
fieldName=tag
# --field-end--
# --field-start--
fieldName=id
columnName=_id
generatedId=true
# --field-end--
# --field-start--
fieldName=product
canBeNull=false
foreign=true
# --field-end--
# --field-start--
fieldName=createdts
# --field-end--
# --field-start--
fieldName=modifiedts
# --field-end--
# --field-start--
fieldName=updatecounter
# --field-end--
# --field-start--
fieldName=deleted
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.storely.entities.stores.UpdatableEntity
tableName=updatableentity
# --table-fields-start--
# --field-start--
fieldName=updatecounter
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.storely.entities.stores.UploadableEntity
tableName=uploadableentity
# --table-fields-start--
# --field-start--
fieldName=createdts
# --field-end--
# --field-start--
fieldName=modifiedts
# --field-end--
# --field-start--
fieldName=updatecounter
# --field-end--
# --field-start--
fieldName=deleted
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.storely.entities.stores.User
tableName=users
# --table-fields-start--
# --field-start--
fieldName=mailid
columnName=_id
canBeNull=false
id=true
# --field-end--
# --field-start--
fieldName=name
# --field-end--
# --field-start--
fieldName=mobileno
# --field-end--
# --field-start--
fieldName=createdts
# --field-end--
# --field-start--
fieldName=modifiedts
# --field-end--
# --field-start--
fieldName=updatecounter
# --field-end--
# --field-start--
fieldName=deleted
# --field-end--
# --table-fields-end--
# --table-end--
#################################
